import { createApp } from 'vue'
import App from './App.vue'


import Antd from 'ant-design-vue';
import 'ant-design-vue/dist/antd.css';

import VMdEditor from '@kangc/v-md-editor';
import '@kangc/v-md-editor/lib/style/base-editor.css';
import githubTheme from '@kangc/v-md-editor/lib/theme/github.js';
import '@kangc/v-md-editor/lib/theme/style/github.css';

// highlightjs
import hljs from 'highlight.js';

VMdEditor.use(githubTheme, {
    Hljs: hljs,
});


const app = createApp(App);
app.config.productionTip = false;

app.use(Antd).use(VMdEditor).mount('#app')
