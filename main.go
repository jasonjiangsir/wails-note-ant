package main

import (
	"embed"
	"log"

	"github.com/wailsapp/wails/v2"
	"github.com/wailsapp/wails/v2/pkg/logger"
	"github.com/wailsapp/wails/v2/pkg/options"
	"github.com/wailsapp/wails/v2/pkg/options/windows"
)

// 注解，指定资源路径，编译时将静态资源文件打包进编译好的程序中
//go:embed frontend/dist
var assets embed.FS

func main() {
	// Create an instance of the app structure
	app := NewApp()

	// Create application with options
	err := wails.Run(&options.App{
		Title:     "wails note",
		Width:     1366,
		Height:    768,
		MinWidth:  1366,
		MinHeight: 768,
		// MaxWidth:          1366,
		// MaxHeight:         768,
		DisableResize:     false, // 不可调节窗口大小
		Fullscreen:        false, // 是否全屏
		Frameless:         false, // 无边框
		StartHidden:       false, // 启动隐藏
		HideWindowOnClose: false, // 退出隐藏窗口
		RGBA:              &options.RGBA{R: 255, G: 255, B: 255, A: 255},
		Assets:            assets,
		LogLevel:          logger.DEBUG,
		OnStartup:         app.startup,  // 程序启动回调
		OnDomReady:        app.domReady, // 程序加载完毕回调
		OnShutdown:        app.shutdown, // 程序退出回调
		Bind: []interface{}{
			app,
		},
		// Windows platform specific options
		Windows: &windows.Options{
			WebviewIsTransparent: false,
			WindowIsTranslucent:  false,
			DisableWindowIcon:    false,
		},
	})

	if err != nil {
		log.Fatal(err)
	}
}
