export interface go {
  "main": {
    "App": {
		CreateNoteFile(arg1:string):Promise<RespDate>
		CreateNotebook(arg1:string):Promise<RespDate>
		DownToLocal():Promise<RespDate>
		GetDirs():Promise<RespDate>
		GetJGNoteList(arg1:string):Promise<RespDate>
		GetJGNotebook():Promise<RespDate>
		ReadNoteFile(arg1:string,arg2:string):Promise<RespDate>
		RemoveNote(arg1:string,arg2:string):Promise<RespDate>
		RemoveNotebook(arg1:string):Promise<RespDate>
		RemoveTempNote(arg1:string,arg2:string):Promise<RespDate>
		SaveNote(arg1:string,arg2:string,arg3:string,arg4:string):Promise<RespDate>
		SyncToCloud():Promise<RespDate>
    },
  }

}

declare global {
	interface Window {
		go: go;
	}
}
