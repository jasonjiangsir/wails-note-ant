# wails-note-ant

#### 介绍
wails-note的Ant Design Vue版本  
基于wails的笔记小程序，支持上传笔记到坚果云  
go + vue3 + Ant Design Vue  

element plus版本请移步（不维护了）： [wails-note](https://gitee.com/yuguofu/wails-note)  

#### 软件架构
wails + vue3 + Ant Design Vue  

wails官网： https://wails.app/  
wails中文镜像网站： https://wails.top/zh-Hans/  

#### 安装教程

1.  前端frontend目录下执行`npm install && npm run build`安装依赖和编译  
2.  wails.json同级目录下执行 `wails dev`调试  
3.  wails.json同级目录下执行 `wails build`打包
>  _windows无调试信息打包建议命令_ ： `wails build -tags desktop,production -ldflags "-w -s -h -H windowsgui" -webview2 embed`  

>  _linux无调试信息打包建议命令_ ： `wails build -tags desktop,production -ldflags "-w -s" `  

#### 使用说明

1.  前端frontend目录下执行`npm install && npm run build`安装依赖和编译  
2.  wails.json同级目录下执行 `wails dev`调试  
3.  wails.json同级目录下执行 `wails build`打包
>  _windows无调试信息打包建议命令_ ： `wails build -tags desktop,production -ldflags "-w -s -h -H windowsgui" -webview2 embed`  

>  _linux无调试信息打包建议命令_ ： `wails build -tags desktop,production -ldflags "-w -s" `  


#### 截图
![添加设置功能1](%E6%88%AA%E5%9B%BE/v1.2-1.png)  
![添加设置功能2](%E6%88%AA%E5%9B%BE/v1.2-2.png)  
![输入图片说明](%E6%88%AA%E5%9B%BE/QQ%E6%88%AA%E5%9B%BE20220307213802.png)
![输入图片说明](%E6%88%AA%E5%9B%BE/QQ%E6%88%AA%E5%9B%BE20220307213325.png)
![输入图片说明](%E6%88%AA%E5%9B%BE/image.png)